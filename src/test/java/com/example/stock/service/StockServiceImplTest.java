//package com.example.stock.service;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import com.example.stock.constant.ApplicationConstant;
//import com.example.stock.dto.QuantityDto;
//import com.example.stock.dto.QuantityResponse;
//import com.example.stock.dto.StockDto;
//import com.example.stock.entity.Stock;
//import com.example.stock.exception.InvalidInput;
//import com.example.stock.exception.StockNotFoundException;
//import com.example.stock.repository.StockRepository;
//
//import lombok.extern.slf4j.Slf4j;
//
//@SpringBootTest
//@Slf4j
//class StockServiceImplTest {
//	@InjectMocks
//	StockServiceImpl stockServiceImpl;
//	@Mock
//	StockRepository stockRepository;
//	List<Stock> stockList = new ArrayList<Stock>();
//	Stock stock;
//
//	QuantityDto qu = new QuantityDto("buy", 12l, 1l);
//
//	@BeforeEach
//	/*
//	 * public void init() { stock = new Stock(1L, "hcl", 12500.00, 500.05,
//	 * "+500.05", 20L, LocalDateTime.now()); stockList = Stream.of(new Stock(1L,
//	 * "hcl", 12500.00, 500.05, "+500.05", 20L, LocalDateTime.now()))
//	 * .collect(Collectors.toList());
//	 * 
//	 * }
//	 */
//
//	@Test
//	public void getAllStock() {
//
//		Mockito.when(stockRepository.findAll()).thenReturn(stockList);
//		List<StockDto> allStocks = stockServiceImpl.getAllStocks();
//		Assertions.assertEquals(1, allStocks.size());
//	}
//
//	@Test
//	public void getAllStockNegative() {
//		List<Stock> stockLists = new ArrayList<Stock>();
//		try {
//			Mockito.when(stockRepository.findAll()).thenReturn(stockLists);
//			List<StockDto> allStocks = stockServiceImpl.getAllStocks();
//		} catch (StockNotFoundException stockex) {
//			Assertions.assertEquals(ApplicationConstant.STOCK_NOT_FOUND, stockex.getLocalizedMessage());
//
//		}
//
//	}
//
//	@Test
//	public void updateQuantity() {
//		Mockito.when(stockRepository.findById(1L)).thenReturn(Optional.of(stock));
//		QuantityResponse updateStock = stockServiceImpl.updateStock(qu);
//		Assertions.assertEquals(ApplicationConstant.SUCCESS_STATUS, updateStock.getStatusCode());
//
//	}
//
//	@Test
//	public void updateQuantityNegative() {
//		try {
//			Mockito.when(stockRepository.findById(2L)).thenReturn(Optional.of(stock));
//			QuantityResponse updateStock = stockServiceImpl.updateStock(qu);
//		} catch (StockNotFoundException stockex) {
//			Assertions.assertEquals(ApplicationConstant.STOCK_NOT_FOUND, stockex.getLocalizedMessage());
//
//		}
//
//	}
//
//	@Test
//	public void updateQuantitSell() {
//		QuantityDto qu = new QuantityDto("sell", 12l, 1l);
//		Mockito.when(stockRepository.findById(1L)).thenReturn(Optional.of(stock));
//		log.info(" selling the stock {} ", qu.getStockId());
//		QuantityResponse updateStock = stockServiceImpl.updateStock(qu);
//		Assertions.assertEquals(ApplicationConstant.SUCCESS_STATUS, updateStock.getStatusCode());
//
//	}
//
//	@Test
//	public void QuantityGreater() {
//		try {
//			QuantityDto qu = new QuantityDto("sell", 80l, 1l);
//			Mockito.when(stockRepository.findById(1L)).thenReturn(Optional.of(stock));
//			QuantityResponse updateStock = stockServiceImpl.updateStock(qu);
//		} catch (InvalidInput invalid) {
//			Assertions.assertEquals(ApplicationConstant.QUANTITY, invalid.getLocalizedMessage());
//		}
//	}
//
//	@Test
//	public void transectionType() {
//		try {
//			QuantityDto qu = new QuantityDto("anything", 80l, 1l);
//			Mockito.when(stockRepository.findById(1L)).thenReturn(Optional.of(stock));
//			QuantityResponse updateStock = stockServiceImpl.updateStock(qu);
//		} catch (InvalidInput invalid) {
//			Assertions.assertEquals(ApplicationConstant.INVALID_TRANSACTION, invalid.getLocalizedMessage());
//		}
//	}
//
//}

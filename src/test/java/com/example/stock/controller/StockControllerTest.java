//package com.example.stock.controller;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//
//import com.example.stock.constant.ApplicationConstant;
//import com.example.stock.dto.QuantityDto;
//import com.example.stock.dto.QuantityResponse;
//import com.example.stock.dto.StockDto;
//import com.example.stock.service.StockServiceImpl;
//import com.google.common.math.Quantiles;
//@SpringBootTest
//public class StockControllerTest {
//
//	@InjectMocks
//	StockController stockController;
//	@Mock
//	StockServiceImpl stockServiceImpl;
//	
//	List<StockDto> stockdto =new ArrayList<>();
//	QuantityDto qua=new QuantityDto();
//	QuantityResponse qur=new QuantityResponse();
//	@BeforeEach
//	public void init() {
//		qur.setMessage(ApplicationConstant.SUCCESS);
//		qur.setStatusCode(ApplicationConstant.SUCCESS_STATUS);
//		stockdto=Stream.of(new StockDto(1L, "hcl", 12500.00, 500.05, "+500.05", 20L )).collect(Collectors.toList());
//	}
//	@Test
//	public void getAllStock() {
//	Mockito.when(stockServiceImpl.getAllStocks()).thenReturn(stockdto);
//	ResponseEntity<List<StockDto>> allStocks = stockController.getAllStocks();
//	Assertions.assertEquals(allStocks.getStatusCodeValue(), HttpStatus.OK.value());
//	}
//	@Test
//	public void updateQuantity() {
//		Mockito.when(stockServiceImpl.updateStock(qua),1l).thenReturn(qur);
//		ResponseEntity<QuantityResponse> updateStock = stockController.updateStock(qua);
//		Assertions.assertEquals(ApplicationConstant.SUCCESS_STATUS, updateStock.getBody().getStatusCode());
//	}
//	
//}

package com.example.stock.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.stock.dto.QuantityDto;
import com.example.stock.dto.QuantityResponse;
import com.example.stock.dto.StockDto;
import com.example.stock.entity.Stock;

@Service
public interface StockService {

	public List<StockDto> getAllStocks();

	public Stock updateStock(Long quantity,Long stockid);

	public Stock getByidStocks(Long stockid);

	public String deleteStock(Long stockid);
}

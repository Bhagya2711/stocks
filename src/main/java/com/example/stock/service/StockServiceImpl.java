package com.example.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.stock.constant.ApplicationConstant;
import com.example.stock.constant.TransactionType;
import com.example.stock.dto.QuantityDto;
import com.example.stock.dto.QuantityResponse;
import com.example.stock.dto.StockDto;
import com.example.stock.entity.Stock;
import com.example.stock.exception.InvalidInput;
import com.example.stock.exception.StockNotFoundException;
import com.example.stock.repository.StockRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StockServiceImpl implements StockService {

	@Autowired
	StockRepository stockRepository;

	@Override
	public List<StockDto> getAllStocks() {
		log.info("Stock Service: get all stocks ");

		List<Stock> findAll = stockRepository.findAll();

		if (findAll.isEmpty()) {
			log.info(" stock not found");
			throw new StockNotFoundException(ApplicationConstant.STOCK_NOT_FOUND);
		}

		List<StockDto> stockDto = new ArrayList<>();
		findAll.forEach(temp -> {
			StockDto stock = new StockDto();
			BeanUtils.copyProperties(temp, stock);
			stockDto.add(stock);

		});
		return stockDto;
	}

	@Override
	@Transactional
	@CachePut(value = "stocks", key = "#stockid")
	public Stock updateStock(Long quantity, Long stockid) {
		log.info("Stock Service: updating stocks ");
		Optional<Stock> findById = stockRepository.findById(stockid);
		if (!findById.isPresent()) {
//			log.info("stock id not found {} ", quantityDto.getStockId());

			throw new StockNotFoundException(ApplicationConstant.STOCK_NOT_FOUND);
		}

		Stock stock = findById.get();

		/*
		 * if (quantityDto.getTransactionType().equalsIgnoreCase(TransactionType.BUY.
		 * getTransaction())) { if (quantityDto.getQuantity() >
		 * stock.getAvailableQuantity()) {
		 * log.info("quantity is greater than excepted {} ", quantityDto.getQuantity());
		 * throw new InvalidInput(ApplicationConstant.QUANTITY); }
		 */
//			log.info(" buying the stock {}", quantityDto.getStockId());
		Long temp = stock.getAvailableQuantity() - quantity;

		Stock so = new Stock();
		BeanUtils.copyProperties(stock, so);
		so.setAvailableQuantity(temp);

		stockRepository.save(so);
		return so;

//		}
//		if (quantityDto.getTransactionType().equalsIgnoreCase(TransactionType.SELL.getTransaction())) {
//			log.info(" selling the stock {} ", quantityDto.getStockId());
//			Long temp = stock.getAvailableQuantity() + quantityDto.getQuantity();
//
//			Stock so = new Stock();
//
//			BeanUtils.copyProperties(stock, so);
//			so.setAvailableQuantity(temp);
//			stockRepository.save(so);
//			return new QuantityResponse(ApplicationConstant.SUCCESS_STATUS, ApplicationConstant.SUCCESS);
//		}

//		else {
//			log.info("invalid inputs like : ", quantityDto.getTransactionType());
//			throw new InvalidInput(ApplicationConstant.INVALID_TRANSACTION);
//		}

	}

	@Override
	@Cacheable(value = "stocks", key = "#stockid")
	public Stock getByidStocks(Long stockid) {
		Optional<Stock> findById = stockRepository.findById(stockid);
		if (!findById.isPresent()) {
			throw new StockNotFoundException("invalid inputs");
		}
		return findById.get();
	}

	@Override
	@CacheEvict(value = "stocks", allEntries = true)
	@Transactional
	public String deleteStock(Long stockid) {
		Optional<Stock> findById = stockRepository.findById(stockid);
		if (!findById.isPresent()) {
			throw new StockNotFoundException("invalid inputs");
		}

		stockRepository.deleteById(stockid);
		return "data deleted";
	}

}

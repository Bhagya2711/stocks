package com.example.stock.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.stock.dto.QuantityDto;
import com.example.stock.dto.QuantityResponse;
import com.example.stock.dto.StockDto;
import com.example.stock.entity.Stock;
import com.example.stock.exception.StockNotFoundException;
import com.example.stock.repository.StockRepository;
import com.example.stock.service.StockService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/stocks")
@RestController
@Slf4j
public class StockController {
	@Autowired
	StockService stockService;

	@Autowired
	StockRepository stockRepo;

	/**
	 * 
	 * @param null
	 *
	 * @return list of StockDetailsDto
	 * @author rakeshr.ra
	 */
	@ApiOperation(value = "get all stocks")
	@GetMapping
	public ResponseEntity<List<StockDto>> getAllStocks() {
		log.info(" StocksController: get all stocks avaliable");
		return new ResponseEntity<>(stockService.getAllStocks(), HttpStatus.OK);

	}

	@GetMapping("/{stockid}")
	public ResponseEntity<Stock> getByidStocks(@PathVariable("stockid") Long stockid) {
		log.info(" StocksController: get all stocks avaliable");
		return new ResponseEntity<>(stockService.getByidStocks(stockid), HttpStatus.OK);

	}

	/**
	 * 
	 * @param stockId
	 * @param transactiontype
	 * @param userId
	 * @return quantityResponse
	 * @author rakeshr.ra
	 */

	@PutMapping("/{quantity}/id/{stockid}")
	public ResponseEntity<String> updateStock(@PathVariable("quantity") Long quantity,
			@PathVariable("stockid") Long stockid) {
		log.info("StocksController: updatingStock ");
		stockService.updateStock(quantity, stockid);
		return new ResponseEntity<>("data updated", HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<String> addStock(@RequestBody Stock quantityDto) {
		log.info("StocksController: updatingStock ");

//		Stock stock=new Stock();
//		BeanUtils.copyProperties(quantityDto, stock);
		stockRepo.save(quantityDto);

		return new ResponseEntity<>("data saved", HttpStatus.OK);

	}

	@DeleteMapping("/{stockId}")
	public ResponseEntity<String> deleteStock(@PathVariable("stockId") Long stockId) {
		log.info("StocksController: updatingStock ");

		return new ResponseEntity<>(stockService.deleteStock(stockId), HttpStatus.OK);

	}

}

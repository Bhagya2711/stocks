/**
 * 
 */
package com.example.stock.constant;

/**
 * @author rakeshr.ra
 *
 */
public enum TransactionType {
	BUY("Buy"), SELL("Sell");

	private String transaction;

	TransactionType(String transaction) {
		this.transaction = transaction;
	}

	public String getTransaction() {
		return transaction;
	}
	
	

}

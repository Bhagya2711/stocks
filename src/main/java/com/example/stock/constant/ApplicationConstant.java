package com.example.stock.constant;
/**
 * @author rakeshr.ra
 *
 */
public class ApplicationConstant {

	public static final String STOCK_NOT_FOUND = "stock not found";
	public static final Integer STOCK_NOT_FOUND_STATUS = 1001;
	public static final String SUCCESS = "success";
	public static final Integer SUCCESS_STATUS = 1002;
	
	public static final String QUANTITY="please check the avaliable quantity then try out";
	public static final Integer QUANTITY_STATUS =1003;
	public static final String INVALID_TRANSACTION=" invalid inputs";
	
	private ApplicationConstant() {}
	
	

}

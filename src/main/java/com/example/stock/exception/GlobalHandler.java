package com.example.stock.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.stock.constant.ApplicationConstant;

@ControllerAdvice
public class GlobalHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(StockNotFoundException.class)
	public ResponseEntity<ErrorStatus> handleStockNotFound(StockNotFoundException stockExcept) {
		ErrorStatus errorStatus = new ErrorStatus();
		errorStatus.setMessage(stockExcept.getLocalizedMessage());
		errorStatus.setStatusCode(ApplicationConstant.STOCK_NOT_FOUND_STATUS);
		return new ResponseEntity<>(errorStatus, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(InvalidInput.class)
	public ResponseEntity<ErrorStatus> handleInvalidInputs(InvalidInput invalid) {
		ErrorStatus errorStatus = new ErrorStatus();
		errorStatus.setMessage(invalid.getLocalizedMessage());
		errorStatus.setStatusCode(ApplicationConstant.QUANTITY_STATUS);
		return new ResponseEntity<>(errorStatus, HttpStatus.BAD_REQUEST);

	}
}
